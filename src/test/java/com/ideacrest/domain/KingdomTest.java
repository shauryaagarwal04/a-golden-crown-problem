package com.ideacrest.domain;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class KingdomTest {

	Kingdom kingdom = new Kingdom("Air","panda");
	
	@DisplayName("Secret message is smaller than emblem or empty")
	@Test
	public void testCheckSecretMessageContainsEmblem_forEmptyMsg_or_SmallerMsg() {
		assertFalse(kingdom.checkSecretMessageContainsEmblem(""));
		assertFalse(kingdom.checkSecretMessageContainsEmblem("rong"));
	}
	
	@DisplayName("Secret message doesnt contain emblem")
	@Test
	public void testCheckSecretMessageContainsEmblem() {
		assertTrue(kingdom.checkSecretMessageContainsEmblem("letsp swinga then sword togethera"));
	}
	
	
}
