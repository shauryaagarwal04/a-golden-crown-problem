package com.ideacrest.domain;

import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringJoiner;

public class Universe {
	private String name;
	private Set<Kingdom> kingdoms;
	private King ruler;
	private static final int minimumNoOfAlliesToBeRuler = 3;
	
	public Universe() {
		this.name = "Southeros"; // tested
		this.kingdoms = new HashSet<Kingdom>();
		kingdoms.add(new Kingdom("land","panda"));
		kingdoms.add(new Kingdom("water","octupus"));
		kingdoms.add(new Kingdom("fire","dragon"));
		kingdoms.add(new Kingdom("ice","mammoth"));
		kingdoms.add(new Kingdom("air","owl"));
		kingdoms.add(new Kingdom("space","gorilla"));
		}
	public void determineRuler(King king) {
		if( king.getAllies().size() >= minimumNoOfAlliesToBeRuler)
			ruler = king ;
		
	}
	public Set<Kingdom> getKingdoms(){
		return kingdoms;
	}
	public void printUniverseStatus() {
		if(ruler==null) {
		System.out.println("Who is the ruler of Southeros? ");
		System.out.println("None");
		System.out.println("Allies of King Shan? ");
		System.out.println("None");
		}
		else {
	    System.out.println("Who is the ruler of Southeros? ");
		System.out.println(ruler.getName());
		System.out.println("Allies of King Shan? ");
	    StringJoiner allAllies = new StringJoiner(",");
		for(Kingdom ally : ruler.getAllies()) {
			allAllies.add(ally.getName());
		}
		System.out.println(allAllies.toString());
		}
	}
		
	public void populateSecretMessages(King king) {
		
	    Scanner sc = new Scanner(System.in);
		
		Map<Kingdom, String> kingdomAndMessages = king.getSecretMessages();
		
		for (Kingdom kingdom : kingdoms) {
			System.out.println("Enter message for Kingdom " + kingdom.getName());
			String secretmessage = sc.nextLine();
			kingdomAndMessages.put(kingdom,secretmessage);
		}
		
	}
	
	
}
