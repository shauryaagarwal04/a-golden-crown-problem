package com.ideacrest.domain;

import java.util.Arrays;

public class Kingdom {
    private String name;
    private King king;
    private String emblem;
    
    Kingdom(String name,String emblem)
    {
    	this.name=name;
    	this.emblem=emblem;
    }
    
    public String getName()
    {
    	return name;
    }
    
   
    public boolean checkSecretMessageContainsEmblem(String message) {
		
		if (message == null || message.isEmpty() || (emblem.length() > message.length()) ) 
           return false;
		else {
			char[] secretMsgChars = message.toCharArray();
            Arrays.sort(secretMsgChars);
			String sortedSecretMsg = String.valueOf(secretMsgChars);
			char[] emblemChars = emblem.toCharArray();
			for (char emblemChar : emblemChars) {
				int indexOfSearchedChar = Arrays.binarySearch(secretMsgChars, emblemChar);
				if(indexOfSearchedChar <= -1) {			
					return false;
				}
				secretMsgChars = sortedSecretMsg.replaceFirst(String.valueOf(emblemChar), "").toCharArray();
				sortedSecretMsg = String.valueOf(secretMsgChars);
			}
			return true;
		}
     }
}

