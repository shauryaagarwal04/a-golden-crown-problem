package com.ideacrest.domain;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class King {
	private String name;
    private Map<Kingdom,String> kingdomMessagesMap ;
    private Set<Kingdom> allies;
    
    public King(String name) {
		this.name =name;
		this.kingdomMessagesMap = new HashMap();
		this.allies = new HashSet();
		
	}
    public String getName()
    {
    	return name;
    }
    
    public Set<Kingdom> getAllies()
    {
    	
    	return allies;
    }
    public Map<Kingdom,String> getSecretMessages() {
    	return kingdomMessagesMap;
    }

	

}
