package com.ideacrest.app;

import java.util.*;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import com.ideacrest.domain.King;
import com.ideacrest.domain.Kingdom;
import com.ideacrest.domain.Universe;

public class AppLauncher {
	public static void main(String[] args) {
		Universe universe = new Universe();
		King kingShan = new King("King Shan");
		
		universe.printUniverseStatus();
		universe.populateSecretMessages(kingShan);
		
		//loop through King MAp
		for (Map.Entry<Kingdom, String> entry : kingShan.getSecretMessages().entrySet()) { 
            if (entry.getKey().checkSecretMessageContainsEmblem(entry.getValue())) {
            	kingShan.getAllies().add(entry.getKey());
            }
		}
		universe.determineRuler(kingShan);
		//for each secret msg call Kingdoms checksecretemblem
		//if check secret msg true add Kingdom as ally to kingshan 
		//call determine ruler , if true set universe ruler as King Shan
		universe.printUniverseStatus();
		}
		
}


